package recta;

import java.util.Scanner;
/**
 *
 * @author ASE Ü
 */
public class Recta {
    
    int A;
    int B;
    int C;
    /**
     * Constructor por parametros
     */
    
    Recta(int x, int y,int z){
        this.A=x;
        this.B=y;
        this.C=z;
    }
    
    private void setA(int a){
        this.A=a;
    }
    private void setB(int b){
        this.B=b;
    }
    private void setC(int c){
        this.C=c;
    }
    private int getA(){
        return this.A;
    }
    private int getB(){
        return this.B;
    }
    private int getC(){
        return this.C;
    }
    void show(){
        System.out.println("("+getA()+")x +("+getB()+")y +("+getC()+")");
    }
    
    /**
     * Método que transforma la forma general de la recta
     * en su forma punto-pendiente
     * Ejercicio 3. Elaborado por Aseret Guzmán Luna
     */
    void dosPuntos(Recta r){
        int x1=0;
        float y1=-r.getC()/r.getB();
        float x2=-r.getC()/r.getA();
        int y2=0;
        System.out.println("Ecuación que pasa por dos puntos: ");
        System.out.println("Puntos: ");
        System.out.println("P1 = ("+x1+","+y1+") P2 = ("+x2+","+y2+")");
        System.out.println("Ecuacion =>  y-("+y1+") = [("+y2+"-("+y1+"))/(("+x2+")-"+x1+")](x-"+x1+")");
        }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int x,y,z;
        Scanner valores=new Scanner(System.in);
        System.out.println(">>Transformaciones de la forma general de la Recta<<");
        System.out.println("Cuales son los parámetros para X, Y y Z?");
        x=valores.nextInt();
        y=valores.nextInt();
        z=valores.nextInt();
        Recta r=new Recta(x,y,z); 
         System.out.println("Tu ecuación en su forma general es:");
         r.show();
         r.dosPuntos(r);
    }
    
}
